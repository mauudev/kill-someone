const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const ScoreSchema = mongoose.Schema({
    kills: Number,
    deaths: Number,
    player: { type: Schema.ObjectId, ref: "Player" } 
}, {
    timestamps: true
});

module.exports = mongoose.model('Score', ScoreSchema);
